package com.mastertech.marketplace.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.models.Offer;
import com.mastertech.marketplace.repositories.OfferRepository;
import com.mastertech.marketplace.repositories.ProductRepository;

@RestController
@RequestMapping("/offers")
public class OfferController {
	@Autowired
	OfferRepository offerRepository;

	@Autowired
	ProductRepository productRepository;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<?> getAll() {
		return offerRepository.findAll();
	}

	@RequestMapping(path="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable int id) {
		Optional<Offer> offerQuery = offerRepository.findById(id);
		
		if(offerQuery.isPresent()) {
			Offer offer = offerQuery.get();
			return ResponseEntity.ok(offer);
		}
		
		return ResponseEntity.notFound().build();
	}

	//Adicionar método para criação de uma oferta
	
	//Adicionar método para aprovação de uma oferta
	
	//Adicionar método para listagem de ofertas de um produto
}
